package taskapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class TaskApp {
	// array de tareas
	static String[] a = { "Estudiar examen", "Práctica Entornos", "Encontrar trabajo" };
	static ArrayList<Task> tasks = new ArrayList<Task>();
	static ArrayList<Task> done = new ArrayList<Task>();
	
	// entrada por consola
	static Scanner sc;

	public static void main(String[] args) {
		//cargar tareas
		loadTasks();
		
		// inicializar scanner
		sc = new Scanner(System.in);

		int option = -1;

		// ejecutar mientras no elijamos salir
		while (option != 0) {
			// mostrar tareas
			System.out.println("\nTAREAS\n------\n");
			if (tasks.size() > 0) {
				for (int i = 0; i < tasks.size(); i++) {
					System.out.println((i + 1) + " - " + tasks.get(i));
				}
			} else {
				System.out.println("No hay tareas pendientes");
			}
			// mostrar opciones
			System.out.println("\nOpciones:");
			System.out.println("1 - Tarea hecha");
			System.out.println("2 - Nueva tarea");
			System.out.println("3 - Ver tareas hechas");
			System.out.println("4 - Guardar tareas");
			System.out.println("5 - Editar tareas");
			System.out.println("6 - Borrar tareas");
			System.out.println("7 - Ver tareas pendientes con fecha de creacion");
			System.out.println("8 - Ver tareas hechas con fecha de realizacion");
			System.out.println("0 - Salir");
			
			option = askInt("Selecciona una opción");
			
			switch (option) {
			case 1:
				doTask();
				break;
			case 2:
				addTask();
				break;
			case 3:
				showDone();
				break;
			case 4:
				saveTasks();
				break;
			case 5:
				editTask();
				break;
			case 6:
				deleteTask();
				break;
			case 7:
				showTask();
				break;
			case 8:
				showTaskDone();
				break;
			case 0:
				break;
			default:
				System.out.println("Opción invalida");
				break;
			}
			
		}

		// close scanner
		sc.close();
	}

	private static void showTask() {
		for (int i = 0; i < tasks.size(); i++) {
			System.out.println((i + 1) + " - " + tasks.get(i) + " - " + tasks.get(i).getCreation());
		}
		
	}

	private static void showTaskDone() {
		for (int i = 0; i < done.size(); i++) {
			System.out.println((i + 1) + " - " + done.get(i) + " - " + done.get(i).getDone());
		}
		
	}
	private static void deleteTask() {
		int pos = askInt("Introduce la posicion de la tarea");
		pos = pos - 1;
		if (pos >0 && pos < tasks.size()) {
		tasks.remove(pos);
		} else {
			System.out.println("La tarea indicada no existe");
		}
		
	}

	private static void editTask() {
		int pos = askInt("Introduce la posicion de la tarea");
		pos = pos - 1;
		if (pos >0 && pos < tasks.size()) {
		System.out.println("Introduce la nueva descripcion de la tarea");
		String desc = sc.nextLine();
		tasks.get(pos).setDescription(desc);
		} else {
			System.out.println("La tarea indicada no existe");
		}
	}

	private static void showDone() {
		System.out.println();
		System.out.println("Tareas hechas");
		System.out.println("-------------");
		for (Task task : done) {
			System.out.println(task);
		}
		System.out.println("Pulsa ENTRAR para continuar...");
		sc.nextLine();
		
	}

	private static void addTask() {
		// preguntar la tarea
		System.out.println("Introduce la descripción de la nueva tarea");
		String newTask = sc.nextLine();
		
		// añadir la nueva tarea
		tasks.add(new Task(newTask));
	}

	private static void doTask() {
		// pedir tarea hecha
		int pos = askInt("Introduce el número de la tarea hecha");
		pos--; // ajustar al índice del array
		
		//verificar posición correcta
		if(pos<0 || pos>=tasks.size()) {
			System.out.println("El índice no está en la lista");
			return;
		}
		
		//Acceder a la tarea a eliminar
		//y guardarla en la coleccion done
		Task task = tasks.get(pos);
		task.doTask();
		done.add(task);
		
		//eliminar el elemento seleccionado
		tasks.remove(pos);
		
	}

	private static void loadTasks() {
		//ruta del archivo
		
		String ruta = "/home/administrador/Documents/tasks.txt";
        File file = new File(ruta);
        BufferedWriter bw = null;
        if(file.exists()) {
           System.out.println("Ya existe");
        } else {
            try {
				bw = new BufferedWriter(new FileWriter(file));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
       
	
		
		
		//haces lectura del archivo
		try(BufferedReader in 
					= new BufferedReader(new FileReader(file))){
			String line;
			//bucle para leer todas las lineas
			while((line = in.readLine())!=null) {
				//divides el array por dos puntos
				String[] parts = line.split(":");
				//si la tarea se compone de 3 partes, es valida
				if (parts.length == 3) {
					try {
						LocalDate creationDate = LocalDate.parse(parts[1]);
						LocalDate doneDate = null;
						//si la parte de fecha de realizacion es null, guardamos en el array de
						//tareas pendientes
						if (parts[2].equals("null")) {
							Task task = new Task(parts[0], creationDate, doneDate);
							tasks.add(task);
						//si no es null, la guardamos en el array de tareas realizadas
						} else {
							doneDate = LocalDate.parse(parts[2]);
							Task task = new Task(parts[0], creationDate, doneDate);
							done.add(task);
						}
						
					} catch (Exception e) {
						System.out.println(e);
					} 
				} 
				
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	} 
	
	private static void saveTasks() {
		//accedemos al arhivo
		String orig = "/home/administrador/Documents/tasks.txt";
		File file = new File(orig);
		
		//escritura en el archivo
		try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
			//recorre ek array de tareas hechas y escribe la descripcion:creacion:realizacion y
			//salto de linea
			for (int i = 0; i < done.size(); i++) {
				out.print(done.get(i).getDescription() + ":" + done.get(i).getCreation() + ":" + done.get(i).getDone()
						+ "\r\n");
			}
			//Lo mismo con el array de tareas pendientes
			for (int i = 0; i < tasks.size(); i++) {
				out.print(tasks.get(i).getDescription() + ":" + tasks.get(i).getCreation() + ":null\r\n");
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private static int askInt(String msg) {
		// pedimos numero y aseguramos que es int
		System.out.println(msg);
		while (!sc.hasNextInt()) { // aseguramos que hay un int
			sc.nextLine();
			System.out.println(msg);
		}
		// obtener numero del usuario
		int num = sc.nextInt();
		sc.nextLine();
		return num;
	}
}
