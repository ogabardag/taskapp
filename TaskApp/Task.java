package taskapp;

import java.time.LocalDate;

public class Task {
	private String description;
	private LocalDate creation;
	private LocalDate done;
	
	public Task(String description, LocalDate creation, LocalDate done) {
		super();
		this.description = description;
		this.creation = creation;
		this.done = done;
	}

	public Task() {
		super();
		creation = LocalDate.now();
	}

	public Task(String description) {
		this();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getCreation() {
		return creation;
	}

	public LocalDate getDone() {
		return done;
	}
	
	public void doTask() {
		if(done==null) {
			done = LocalDate.now();
		}
	}
	
	public void undoTask() {
		done = null;
	}
	
	@Override
	public String toString() {
		return description;
	}
	
	
}
